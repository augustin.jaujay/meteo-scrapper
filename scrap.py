from bs4 import BeautifulSoup
import hug
import json
import urllib.request
from urllib.parse import quote

def sendResp(status, location, hoursWindow, cumulationTreshold):
    return json.dumps({"status": status, "location": location, "hoursWindow": hoursWindow, "cumulationTreshold": cumulationTreshold}, ensure_ascii=False)

@hug.get('/{location}')
def scrapMeteoBlue(location: str, hoursWindow: int=6, cumulationTreshold: int=10):
    url = 'https://www.meteoblue.com/fr/meteo/outdoorsports/seeing/' + quote(location)
    # url = 'https://www.meteoblue.com/fr/meteo/outdoorsports/seeing/' + quote('plouzané_france_2986626')
    mysite = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(mysite, "html.parser")
    mainHeading = soup.find_all("h1", {"class": "main-heading"})[0].text.strip()
    location = mainHeading.split(None, 2)[2]
    previsionTable = soup.find_all("table", {"class": "table-seeing"})[0]
    hoursPrevision = previsionTable.find_all("tr", {"class": "hour-row"})

    slidingCumulation = []

    for hourPrevision in hoursPrevision[1:]:
        columns = list(hourPrevision.find_all("td"))
        hour = int(columns[0].text.strip())
        lowClouds = int(columns[1].text.strip())
        midClouds = int(columns[2].text.strip())
        highClouds = int(columns[3].text.strip())
        # print(hour, lowClouds, midClouds,  highClouds)

        slidingCumulation.append(lowClouds + midClouds + highClouds)
        # print(slidingCumulation)
        # print(sum(slidingCumulation))
        if (len(slidingCumulation) >= hoursWindow):
            if (sum(slidingCumulation) <= cumulationTreshold):
                # print("Créneau astro !")
                # break
                return sendResp("OK", location, hoursWindow, cumulationTreshold)
            slidingCumulation.pop(0)

    return sendResp("KO", location, hoursWindow, cumulationTreshold)