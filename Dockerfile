FROM python:3.9-alpine

WORKDIR /meteo

RUN pip3 install beautifulsoup4
RUN pip3 install hug

COPY . .

CMD hug -p 8080 -f scrap.py

EXPOSE 8080
